#Annuaire de l'AVMG

## Installation du setup de dev

1. Création du virtualenv : `virtualenv --python=python3 venv`
2. Activation de l'env : `./venv/bin/activate.fish` (si on utilise fish)
2. Installation des paquets nécessaires : `pip install -r requirements_dev.txt`
3. Activation du mode debug : `echo "DEBUG=True" > avmg/settings_dev.py`
4. Tests : `./manage.py test`

## TODO
* Template pour change password
* Générer les lettres avec username
