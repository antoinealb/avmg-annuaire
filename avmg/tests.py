from django.test import TestCase, Client
from django.core.urlresolvers import reverse

class LoginViewTest(TestCase):
    """
    Tests for the various login views.
    """

    fixtures = ['test-data.json']
    def test_login_get(self):
        """
        Tests that GET on login page works.
        """
        c = Client()
        response = c.get('/login/')
        self.assertEqual(200, response.status_code)

    def test_login_post_valid_password_redirects_to_index(self):
        """
        Checks that when we put a valid password we get redirected to the index
        page.
        """
        c = Client()
        data = {}
        data['username'] = 'admin'
        data['password'] = 'test'
        response = c.post('/login/', data=data)

        # 302 Redirected
        self.assertEqual(302, response.status_code)
        self.assertEqual('http://testserver/', response.url)

    def test_logout_view(self):
        """
        Checks that the logout view works and redirects us to the homepage.
        """
        c = Client()
        c.login(username='admin', password='test')

        response = c.get('/logout/')

        self.assertEqual(302, response.status_code)

    def test_404_page(self):
        """
        Checks that the 404 error page works properly.
        """
        c = Client()
        response = c.get('/thispagedoesnotexist/')

        self.assertEqual(404, response.status_code)
        self.assertIn('404.html', [i.name for i in response.templates])

    def test_password_reset(self):
        """
        Checks that we can get the password reset page.
        """
        c = Client()
        c.login(username='admin', password='test')
        response = c.get(reverse('django.contrib.auth.views.password_change'))
        self.assertEqual(200, response.status_code)
