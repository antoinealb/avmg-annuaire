from django.contrib import admin
from annuaire.models import *

class PhoneInline(admin.StackedInline):
    model = PhoneNumber
    extra = 1

class TeacherAdmin(admin.ModelAdmin):
    list_filter = ['schools', 'courses']
    list_display = ['name', 'firstname']
    inlines = [PhoneInline]



admin.site.register(School)
admin.site.register(Course)
admin.site.register(Teacher, TeacherAdmin)
