from django.db import models

class School(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Course(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Teacher(models.Model):
    """
    Class representing a single teacher.

    A teacher has physical attributes, like first name, last name, etc. plus
    relationship to courses and schools.
    A given teacher can teach many courses in many schools.
    """
    name = models.CharField(max_length=200)
    firstname = models.CharField(max_length=200)
    schools = models.ManyToManyField(School)
    courses = models.ManyToManyField(Course)
    street = models.TextField(blank=True)
    city = models.CharField(max_length=200, blank=True)
    zip_code = models.CharField(max_length=5, blank=True)
    country = models.CharField(max_length=20, default="Suisse", blank=True)
    email = models.EmailField(blank=True)

    def __str__(self):
        return self.complete_name()

    def complete_name(self):
        return self.firstname + " " + self.name

class PhoneNumber(models.Model):
    phone = models.CharField(max_length=20)
    teacher = models.ForeignKey(Teacher)

    def __str__(self):
        return self.phone
