from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.core.urlresolvers import reverse

from urllib.parse import urlencode

from annuaire.models import *

import csv
import io

def teachers_for_request(request):
    """
    Returns a set of all Teachers matching the GET parameters in the given
    request.
    """
    results = Teacher.objects.order_by('name')

    if 'name' in request.GET:
        results = results.filter(name__icontains=request.GET['name'])

    if 'course' in request.GET and request.GET['course']:
        results = results.filter(courses__pk=request.GET['course'])

    if 'school' in request.GET and request.GET['school']:
        results = results.filter(schools__pk=request.GET['school'])

    return results

@login_required
def index(request):
    context = {}
    context['schools'] = School.objects.all().order_by('name')
    context['courses'] = Course.objects.all().order_by('name')
    return render(request,'annuaire/index.html', context)

@login_required
def search(request):
    context = {'teachers':teachers_for_request(request)}
    context['download_url'] = reverse('download') + '?' + urlencode(request.GET)
    return render(request, 'annuaire/search.html', context)

@login_required
def download(request):
    output = io.StringIO()
    writer = csv.writer(output)

    for teacher in teachers_for_request(request):
        row = []
        row.append(teacher.name)
        row.append(teacher.firstname)
        row.append(teacher.street)
        row.append(teacher.zip_code)
        row.append(teacher.city)
        row.append("\n".join((str(i) for i in teacher.phonenumber_set.all())))
        row.append(teacher.email)
        writer.writerow(row)

    response = HttpResponse(output.getvalue(), content_type='text/csv')

    # Tell the browser it should download the result and save it as
    # annuaire.csv
    response['Content-Disposition'] = 'attachment; filename="annuaire.csv"'
    return response
