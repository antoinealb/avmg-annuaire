from django.test import TestCase, Client

from annuaire.models import *

from bs4 import BeautifulSoup
from io import StringIO
from urllib.parse import *
import csv

class SchoolTest(TestCase):
    """
    All tests for the School model.
    """

    def test_can_create_school(self):
        """
        Checks that we can create a school.
        """
        school = School(name='Gymnase de Burier')
        school.save()
        self.assertEqual(1, len(School.objects.all()))

    def test_has_str_method(self):
        """
        Checks that the School model has a working str method with unicode
        support.
        """
        name = 'Gymnase du Café'
        school = School(name=name)
        self.assertEqual(name, str(school))

class PhoneNumberTestCase(TestCase):
    def test_str(self):
        phone = PhoneNumber(phone="1234")
        self.assertEqual("1234", str(phone))

class CourseTest(TestCase):
    """
    Tests for the Course model.
    """

    def test_can_create_course(self):
        """
        Checks that we can create a course correctly.
        """
        c = Course(name='Anglais')
        c.save()
        self.assertEqual(1, len(Course.objects.all()))

    def test_has_str_method(self):
        """
        Checks that the course str method works properly and supports unicode.
        """
        name = 'Français'
        c = Course(name=name)
        self.assertEqual(name, str(c))

class TeacherTest(TestCase):
    """ All tests for the Teacher model """
    def test_can_add_schools(self):
        """
        Checks that we can create a Teacher and add some school to it (many to
        many relationship).
        """
        school = School(name='Burier')
        teacher = Teacher()

        school.save()
        teacher.save()
        teacher.schools.add(school)

        self.assertEqual(1, len(teacher.schools.all()))

    def test_can_add_courses(self):
        """
        Checks that we can add courses to a given Teacher.
        """
        course = Course(name='Anglais')
        teacher = Teacher()

        teacher.save()
        course.save()

        teacher.courses.add(course)
        self.assertEqual(1, len(teacher.courses.all()))

    def test_can_put_an_adress(self):
        """
        Checks that we can set the adress of a Teacher
        """
        teacher = Teacher()
        teacher.street = "Ch. de l'Auverney 9"
        teacher.zip_code = "1814"
        teacher.city = "La Tour-de-Peilz"
        teacher.save()

        teacher = Teacher.objects.get(pk=1)
        self.assertEqual("Ch. de l'Auverney 9", teacher.street)
        self.assertEqual("1814", teacher.zip_code)
        self.assertEqual("La Tour-de-Peilz", teacher.city)
        self.assertEqual("Suisse", teacher.country)

    def test_str_is_name(self):
        """ Checks that the str() method returns the name. """
        name = "Bolomey"
        first_name = "Jean-Claude"
        teacher = Teacher(name=name, firstname=first_name)
        self.assertEqual("Jean-Claude Bolomey", str(teacher))

    def test_complete_name(self):
        teacher = Teacher(name="Bolomey", firstname="J-C")
        self.assertEqual("J-C Bolomey", teacher.complete_name())

    def test_has_phone_field(self):
        """ Checks that the Teacher Model has a phone field. """
        t = Teacher()
        self.assertEqual(0, len(t.phonenumber_set.all()))

    def test_has_email_field(self):
        """ Checks that the Teacher Model has an email field. """
        teacher = Teacher(email='test@test.com')


class IndexViewTest(TestCase):
    """
    Tests for the home page of the application.
    """

    fixtures = ['test-data.json']

    def setUp(self):
        self.client.login(username='admin', password='test')

    def test_home_is_protected(self):
        """
        Checks that viewing the homepage requires the user to be logged in.
        """
        response = Client().get('/')
        self.assertEqual(302, response.status_code)

    def test_home(self):
        """ Checks that the root view works as expected. """
        response = self.client.get('/')
        self.assertEqual(200, response.status_code)

        schools = response.context['schools']
        courses = response.context['courses']
        self.assertEqual(len(School.objects.all()), len(schools))
        self.assertEqual(len(Course.objects.all()), len(courses))

    def test_course_school_list_are_sorted_by_name(self):
        """
        Checks that the course list and the school list are shown sorted, which
        is way more neat for the end user.
        """
        response = self.client.get('/')

        schools = [i.name for i in response.context['schools']]
        courses = [i.name for i in response.context['courses']]
        self.assertEqual(schools, sorted(schools))
        self.assertEqual(courses, sorted(courses))

    def test_homepage_contains_title(self):
        """
        Checks that the homepage has the propper title.
        """
        response = self.client.get('/')

        # There is a deprecationwarning in Beautifulsoup. Ignore it
        import warnings
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            soup = BeautifulSoup(response.content)

        self.assertEqual("Annuaire de l'AVMG", soup.find('title').text)

class SearchViewTest(TestCase):
    """
    Tests for the search view.
    """
    fixtures = ['test-data.json']

    def setUp(self):
        self.client.login(username='admin', password='test')


    def test_search_is_login_protected(self):
        """
        Checks that the search view is protected by login.
        """
        response = Client().get('/search', data={'course':1})
        self.assertEqual(302, response.status_code)


    def test_search_by_course(self):
        """
        Checks that the search view works when searching by course.
        """
        response = self.client.get('/search', data={'course':1})

        self.assertEqual(200, response.status_code)

        teachers = response.context['teachers']
        self.assertEqual(2, len(teachers))

    def test_search_by_school(self):
        """
        Checks that the search view works when searching by school.
        """
        response = self.client.get('/search', data={'school':1})

        self.assertEqual(200, response.status_code)

        teachers = response.context['teachers']
        self.assertEqual(1, len(teachers))

    def test_search_combined(self):
        """
        Checks that we can search by combining school and course.
        """
        response = self.client.get('/search', data={'school':2,'course':1})

        self.assertEqual(200, response.status_code)

        teachers = response.context['teachers']
        self.assertEqual(0, len(teachers))

    def test_empty_course_is_ignored(self):
        """
        Checks that an empty course field is considered as if there was no
        field.
        """
        response = self.client.get('/search', data={'school':1,'course':''})

        self.assertEqual(200, response.status_code)

        teachers = response.context['teachers']
        self.assertEqual(1, len(teachers))

    def test_empty_school_is_ignored(self):
        """
        Checks that an empty school field is considered as if there was no
        field.
        """
        response = self.client.get('/search', data={'school':'','course':1})

        self.assertEqual(200, response.status_code)

        teachers = response.context['teachers']
        self.assertEqual(2, len(teachers))

    def test_get_by_name(self):
        """
        Checks that we can also search by name.
        """
        response = self.client.get('/search', data={'name':'jac'})
        self.assertEqual(200, response.status_code)
        self.assertEqual('Sebastien Jacquet', response.context['teachers'][0].complete_name())

    def test_get_empty_name_works(self):
        """ Checks that using an empty name doesn't change the results. """
        response = self.client.get('/search', data={'name':'','school':1})
        self.assertEqual(200, response.status_code)
        self.assertEqual('Sebastien Jacquet', response.context['teachers'][0].complete_name())

    def tests_results_are_sorted_by_name(self):
        """
        Checks that the results are sorted by name.
        """
        response = self.client.get('/search', data={'course':1})

        teachers = response.context['teachers']
        teachers_sorted = sorted(teachers, key=lambda s:s.name)

        self.assertEqual(list(teachers_sorted), list(teachers))

    def test_url_for_download(self):
        """
        Checks that the download URL is correctly given to the download page.
        """
        data = {'name':'Jac', 'school':1, 'course':1}
        response = self.client.get('/search', data=data)
        url = response.context['download_url']
        query = parse_qs(urlparse(url).query)
        self.assertIn('name', query)
        self.assertIn('school', query)
        self.assertIn('course', query)


class AdminTestCase(TestCase):
    """
    This class regroups all tests for the admin. It is a LiveServer test case
    because for some reason it doesn't work properly otherwise.
    """
    fixtures = ['test-data.json']

    def setUp(self):
        self.client.login(username='admin', password='test')

    def test_admin_view_teacher(self):
        """
        Checks that the admin view for teachers works properly.
        """
        response = self.client.get('/admin/annuaire/teacher/')
        self.assertEqual(200, response.status_code)

class DownloadViewTestCase(TestCase):

    fixtures = ['test-data.json']

    def setUp(self):
        self.client.login(username='admin', password='test')

    def csv_rows_from_response(self, response):
        self.assertEqual(200, response.status_code)

        with StringIO(response.content.decode('utf-8')) as f:
            reader = csv.reader(f)
            rows = list(reader)

        return rows

    def test_download_is_protected(self):
        """
        Checks that the download page is password protected.
        """
        response = Client().get('/download')
        self.assertEqual(302, response.status_code)

    def test_download_all(self):
        """
        Checks that downloading without any search gives the whole list.
        """
        response = self.client.get('/download')
        rows = self.csv_rows_from_response(response)

        expected = []

        # Teachers must be sorted alphabetically in result file too
        for t in Teacher.objects.order_by('name').all():
            row = []
            row.append(t.name)
            row.append(t.firstname)
            row.append(t.street)
            row.append(t.zip_code)
            row.append(t.city)
            row.append("\n".join((str(i) for i in t.phonenumber_set.all())))
            row.append(t.email)
            expected.append(row)

        self.assertEqual(3, len(rows))

        self.assertEqual(rows, expected)

    def test_can_find_by_school(self):
        """
        Checks that we download a list of all teachers in one school.
        """
        response = self.client.get('/download', data={'name':'','school':1})
        rows = self.csv_rows_from_response(response)
        self.assertEqual(1, len(rows))

    def test_can_find_by_course(self):
        """
        Checks that we can find a list of all teachers giving one course.
        """
        response = self.client.get('/download', data={'name':'','course':1})
        rows = self.csv_rows_from_response(response)
        self.assertEqual(2, len(rows))

    def test_get_by_name(self):
        """
        Checks that we can also search by name.
        """
        response = self.client.get('/download', data={'name':'jac'})
        rows = self.csv_rows_from_response(response)
        self.assertEqual(1, len(rows))

    def test_attachement_name(self):
        """
        Checks that we provide a suggested filename.
        """
        response = self.client.get('/download')
        self.assertEqual(response['Content-Disposition'], 'attachment; filename="annuaire.csv"')
